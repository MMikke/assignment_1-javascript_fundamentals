const balanceElement = document.getElementById("balance");
const payElement = document.getElementById("pay");
const laptopsElement = document.getElementById("laptops");
const imageElement = document.getElementById("image")
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const desciptionElement = document.getElementById("desciption");
const titleElement = document.getElementById("title");
const workElement = document.getElementById("work");
const bankElement = document.getElementById("bank");
const loanElement = document.getElementById("loan");
const loanAmountElement = document.getElementById("loanAmount");
const buyNowElement = document.getElementById("buyNow");

const payLoanElement = document.createElement("button");
payLoanElement.setAttribute("id", "payLoan");
payLoanElement.className = "btn btn-primary";
payLoanElement.style.cssText = "margin-bottom: 5px;"
payLoanElement.innerText = "Pay loan";

let laptops = [];
let pay = 0;
let balance = 0;
let loan = 0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(respons => respons.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToStore(laptops));


const addLaptopsToStore = (laptops) => {
    for (let laptop of laptops){
        let laptopElement = document.createElement("option");
        laptopElement.value = laptop.id;
        laptopElement.appendChild(document.createTextNode(laptop.title));
        laptopsElement.appendChild(laptopElement);
    }
    updateLaptop(laptops[0]);
}

const handleSelectedLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    updateLaptop(selectedLaptop);
}

laptopsElement.addEventListener("change", handleSelectedLaptopChange);

const updateLaptop = (laptop) => {
    updateSpecs(laptop.specs);
    priceElement.innerHTML = "Price: " + laptop.price;
    desciptionElement.innerText = laptop.description;
    titleElement.innerText = laptop.title;
    baseImageSrc = "https://noroff-komputer-store-api.herokuapp.com/";
    imageElement.src = baseImageSrc + laptop.image;
}

const updateSpecs = (specs) => {
    specsElement.innerText = "";
    for (let spec of specs){
        let li = document.createElement("li");
        li.style.cssText = "list-style-type: none;"
        li.appendChild(document.createTextNode(spec));
        specsElement.appendChild(li);
    }
}

const handleWorkButton = () => {
    pay += 100;
    updateBank();
}

workElement.addEventListener("click", handleWorkButton);

const handleBankButton = () => {
    if (loan > 0) {
        let loanPayment = pay * 0.1;
        loan -= loanPayment;
        pay -= loanPayment;
        balance += pay;
        pay = 0;
        if (loan < 0) { 
            balance += (loan * (-1));
            loan = 0;
        }
    }
    else {
        balance += pay;
        pay = 0;
    }
    updateBank();
}

bankElement.addEventListener("click", handleBankButton);

const handleLoanButton = () => {
    if (loan > 0) { 
        alert("You already have a loan");
        return; 
    }
    let amount = prompt("Please enter the amount you wish to loan: ");
    if (!isNaN(parseFloat(amount)) && amount !== null){
        if (amount > (2* balance)){
            alert("You cant loan more then 2X you bank balance");
        }
        else {
            loan = parseFloat(amount);
            balance += parseFloat(loan);
            document.getElementById("workButtonDiv").appendChild(payLoanElement);
            updateBank();
        }
    }
    else {
        alert("Invalid loan amount")
    }
}

loanElement.addEventListener("click", handleLoanButton);

const updateBank = () => {
    balanceElement.innerText = "Balance: " + balance;
    payElement.innerText = "Pay: " + pay;
    loanAmount.innerText = "Loan: " + loan;
    if (loan === 0) {
        payLoanElement.remove();
    }
}


const handlePayLoanButton = () => {
    loan -= pay;
    if (loan < 0){
        loan *= (-1);
        balance += loan;
        loan = 0;
    }
    pay = 0;
    updateBank();
}
payLoanElement.addEventListener("click", handlePayLoanButton);

const handleBuyNowButton = () => {
    let laptop = document.getElementById("laptops");
    laptopIndex = laptop.value - 1;
    laptop = laptops[laptopIndex];
    if (balance < parseFloat(laptop.price)) {
        alert("You cannot afford this computer")
    }
    else {
        balance -= parseFloat(laptop.price);
        updateBank();
        alert(`You are now the happy owner of the new laptop ${laptop.title}`)
    }
}

buyNowElement.addEventListener("click", handleBuyNowButton);